require 'mongo'
require 'yaml'
require "i18n"

CONTENT_TYPE_POST = BSON::ObjectId.from_string("5653704c0f64e321fe000009")
CONTENT_TYPE_OFFER = BSON::ObjectId.from_string("5653704e0f64e321fe000024")
CONTENT_TYPE_PHOTO = BSON::ObjectId.from_string("5653704e0f64e321fe000036")
CONTENT_TYPE_CATEGORY = BSON::ObjectId.from_string("5653704e0f64e321fe000033")
POST_TYPE_OFFER = BSON::ObjectId.from_string("5653704c0f64e321fe000010")

CATEGORY_FLIGHTS = BSON::ObjectId.from_string("565372820f64e321fe0000bb")

DB_CONFIG = YAML.load_file('config/mongoid.yml')[Rails.env.to_s]["clients"]["default"]
DB = "mongodb://#{DB_CONFIG["hosts"].first}/#{DB_CONFIG["database"]}"
DB_SCRIPTS =  "mongodb://localhost:27017/baratrip-scripts"

namespace :db do
  namespace :clean do
    desc "Delete offers that have not been published"
    task :unpublished_offers do
      conn = Mongo::Client.new(DB)
      entries = conn['locomotive_content_entries']
      entries.delete_many({ "content_type_id": CONTENT_TYPE_OFFER, "_visible": false, "created_at": {"$lt" => Date.today - 7} })
      no_offers = entries.find({ "content_type_id": CONTENT_TYPE_OFFER }).count
      types = conn['locomotive_content_types']
      types.update_one({'_id': CONTENT_TYPE_OFFER}, { "$set" => { "number_of_entries" => no_offers }})
    end
  end

  namespace :restructure do

    desc "Tag flight offers according to destination"
    task :import_tag_flight_offers do
      CSV.foreach("/tmp/flights_tags.csv") do |row|
        conn = Mongo::Client.new(DB)
        entries = conn['locomotive_content_entries']
        id = BSON::ObjectId.from_string(row[0])
        tags = row[2]
        if tags
          entries.update_one( { "_id": id }, { "$set" => { "tags" => tags.split(',') } } )
        end
      end
    end

    desc "Tag flight offers according to destination"
    task :extract_tag_flight_offers do
      conn = Mongo::Client.new(DB)
      conn_scripts = Mongo::Client.new(DB_SCRIPTS)
      entries = conn['locomotive_content_entries']
      places = conn_scripts['skyscanner_places']
      offers = entries.find({"content_type_id": CONTENT_TYPE_OFFER, "category_ids": CATEGORY_FLIGHTS}).to_a

      CSV.open("flights_tags.csv", "wb") do |csv|
        offers.each do |offer|
          o_title = offer["title"]
          title = o_title
          title.gsub!(/ida y vuelta/, '')
          matches = title.match(/.*de\s(.*)\sa\s([\wãäëïöüyáéíóúÁÉÍÓÚ\s]*).*\sdesde(.*)/)
          if matches
            o_city = matches[1].strip
            d_city = matches[2].strip
            countries = places.find({city: /#{d_city}/}).to_a.collect{|c| c['country']}.reject { |c| c.blank? }
            countries.uniq!
            raise "More than one country for ''#{title}'? #{countries}" if countries.size > 1
            o_city = I18n.transliterate(o_city)
            d_city = I18n.transliterate(d_city)
            tags = [o_city.downcase, d_city.downcase]
            if countries.size > 0
              d_country = I18n.transliterate(countries[0])
              tags << d_country.downcase if d_country
            end
            csv << [offer['_id'], o_title, tags.join(',')]
          else
            csv << [offer['_id'], o_title]
          end
        end
      end
    end

    desc "Unrelates posts with offers. Then deletes old posts"
    task :clean_old_posts do
      conn = Mongo::Client.new(DB)
      coll = conn['locomotive_content_entries']
      posts = coll.find("content_type_id": CONTENT_TYPE_POST, post_type_id: POST_TYPE_OFFER).to_a

      posts.each do |post|
        post_id = post["_id"]
        coll.update_many( { "content_type_id": CONTENT_TYPE_OFFER, post_id: post_id }, { "$unset" => { "post_id" => "" } } )
        coll.delete_one( { _id: post_id } )
      end
    end

    desc "Moves content from contentType Post to contentType Offer"
    task :posts_to_offers do
      conn = Mongo::Client.new(DB)
      coll = conn['locomotive_content_entries']
      posts = coll.find("content_type_id": CONTENT_TYPE_POST, post_type_id: POST_TYPE_OFFER).to_a

      p "Number of posts with post_type: 'offer': #{posts.size}"

      posts.each do |post|
        post_id = post["_id"]

        # Link offers with categories. It's many_to_many so relation is stored in both sides
        category_ids = post["category_ids"]
        coll.update_many(
          { "content_type_id": CONTENT_TYPE_OFFER, post_id: post_id },
          { "$set" => {
            "category_ids" => category_ids,
            "body" => post["body"],
            "_slug" => post["_slug"],
            "posted_at" => post["posted_at"]
            }
          }
        )
        offers = coll.find("content_type_id": CONTENT_TYPE_OFFER, post_id: post_id).to_a
        offer_ids = offers.collect{|o| o["_id"]}
        coll.update_many( { "_id": {"$in": category_ids}, "content_type_id": CONTENT_TYPE_CATEGORY }, { "$addToSet" => { "offer_ids" => { "$each": offer_ids } } } )

        # Link offers with photos. It's one_to_many, relation is stored in photos
        offer_id = offer_ids.last # Pointing the photo to the latest offer
        coll.update_many( { "content_type_id": CONTENT_TYPE_PHOTO, post_id: post_id }, { "$set" => { "offer_id" => offer_id } } )
      end

    end

    task :clean_duplicated_urls do

      conn = Mongo::Client.new(DB)
      coll = conn['locomotive_content_entries']

      aggregation = coll.aggregate([
         { '$match'=> { 'content_type_id'=> CONTENT_TYPE_OFFER } },
         { '$group'=> { '_id'=> '$post_id', count: { '$sum' => 1 } } }
      ])

      aggregation.each do |doc|
        #=> Yields a BSON::Document.
        if doc["count"] > 1
          offers = coll.find("content_type_id": CONTENT_TYPE_OFFER, post_id: doc["_id"]).sort("_id": 1).to_a
          offer_ids = offers.collect{|o| o["_id"]}
          offer_ids.pop
          coll.update_many( { "_id": {"$in": offer_ids},  "content_type_id": CONTENT_TYPE_OFFER }, { "$set" => { "_visible" => false } } )
        end
      end

    end

  end
end
