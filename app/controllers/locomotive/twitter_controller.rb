require 'open-uri'
require 'json'
module Locomotive

  class TwitterController < Locomotive::ContentEntriesController

    def show
      authorize @content_entry
      begin
        # send("send_#{@content_entry.post_type}_tweet")
        send_offer_tweet
        flash[:success] = "Tweeted"
      rescue Exception => e
        flash[:error] = "Couldn't tweet: #{e.message}"
      end
      redirect_to(:back)
    end

    private

    def client
      creds = JSON.parse(File.read('config/twitter.json'))[Rails.env.to_s]
      Twitter::REST::Client.new do |config|
        config.consumer_key        = creds["consumer_key"]
        config.consumer_secret     = creds["consumer_secret"]
        config.access_token        = creds["access_token"]
        config.access_token_secret = creds["access_token_secret"]
      end
    end

    def send_offer_tweet
      offer = @content_entry
      url = Bitly.client.shorten("http://www.baratrip.es/ofertas/#{offer._slug}").short_url
      if offer.photos.size > 0
        photo = offer.photos[0]
        uri = URI.parse(photo.file.url)
        media = uri.open
        media.instance_eval("def original_filename; '#{File.basename(uri.path)}'; end")
        client.update_with_media("#{offer.title}. Visita #{url} #viajar #ofertas", File.new(media))
      else
        client.update("#{offer.title}. Visita #{url} #viajar #ofertas")
      end
    end

    def send_promocode_tweet
      if @content_entry.has_promocodes?
        promocode = @content_entry.promocodes.last
        url = Bitly.client.shorten("http://www.baratrip.es/descuentos/#{@content_entry._slug}").short_url
        if @content_entry.photos.size > 0
          photo = @content_entry.photos[0]
          uri = URI.parse(photo.file.url)
          media = uri.open
          media.instance_eval("def original_filename; '#{File.basename(uri.path)}'; end")
          client.update_with_media("#{promocode.title}. Visita #{url} #viajar #ofertas", File.new(media))
        else
          client.update("#{promocode.title}. Visita #{url} #viajar #ofertas")
        end
      else
        raise "No promocodes for this post"
      end
    end

    def send_post_tweet
      url = Bitly.client.shorten("http://www.baratrip.es/posts/#{@content_entry._slug}").short_url
      if @content_entry.photos.size > 0
        photo = @content_entry.photos[0]
        uri = URI.parse(photo.file.url)
        media = uri.open
        media.instance_eval("def original_filename; '#{File.basename(uri.path)}'; end")
        client.update_with_media("#{@content_entry.title}. Visita #{url} #viajar #ofertas", File.new(media))
      else
        client.update("#{@content_entry.title}. Visita #{url} #viajar #ofertas")
      end
    end

  end
end
