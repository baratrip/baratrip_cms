class ApplicationMailer < ActionMailer::Base
  default from: "info@baratrip.es"
  layout 'mailer'
end
