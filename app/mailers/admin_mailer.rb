class AdminMailer < ApplicationMailer
  def test_email(test_content)
    mail(to: 'javier.jimenez.villarreal@gmail.com', subject: test_content)
  end
end
