CarrierWave.configure do |config|

  config.cache_dir = File.join(Rails.root, 'tmp', 'uploads')

  case Rails.env.to_sym

  when :development
    config.storage = :file
    config.root = File.join(Rails.root, 'public')

  when :production
    # the following configuration works for Amazon S3
    awsconfig = YAML.load_file('config/aws.yml')['production']
    config.storage          = :aws
    config.aws_bucket       = awsconfig['bucket']
    config.aws_acl          = 'public-read'
    config.aws_credentials  = {
      access_key_id:        awsconfig['access_key_id'],
      secret_access_key:    awsconfig['secret_access_key'],
      region:               awsconfig['region']
    }
    config.asset_host = 'http://d36k8e0u5kt08d.cloudfront.net'
    config.fog_directory  = 'baratrip/'
    config.fog_public     = true

  else
    # settings for the local filesystem
    config.storage = :file
    config.root = File.join(Rails.root, 'public')
  end

end
